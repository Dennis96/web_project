const sqlite3 = require('sqlite3')

const db = new sqlite3.Database("database.db")

db.run(`
	CREATE TABLE IF NOT EXISTS Guestbook(
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		name TEXT,
		email TEXT,
		message TEXT 
		)
`)

db.run(`
	CREATE TABLE IF NOT EXISTS Contact(
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		email TEXT,
		topic TEXT,
		contactMessage TEXT  
		)
`)

db.run(`
 	CREATE TABLE IF NOT EXISTS Project(
 		id INTEGER PRIMARY KEY AUTOINCREMENT,
 		title TEXT,
		projectDescription TEXT,
		dateWhenPosted TEXT 
		)
 `)

exports.getAllPosts = function (callback) {

	const query = "SELECT * FROM Guestbook"

	db.all(query, function (error, guestBookPosts) {
		callback(error, guestBookPosts)
	})
}

exports.createGuestBookPost = function (data, callback) {

	const query = "INSERT INTO Guestbook (name,email,message) VALUES (?, ?, ?)"

	db.run(query, data, function (error) {
		callback(error);
	})
}

exports.deleteGuestBookPost = function (guestBookId, callback) {

	const query = "DELETE FROM Guestbook WHERE id = ?"

	db.run(query, guestBookId, function (error) {
		callback(error);
	})
}

exports.updateGuestBookPost = function (id, name, email, message, callback) {

	const query = "UPDATE Guestbook SET name = ?, email = ?, message = ? WHERE id = ?"

	db.run(query, id, name, email, message, function (error) {
		callback(error)
	})
}

exports.getGuestBookPostById = function (guestBookPostId, callback) {
	
	const query = "SELECT * FROM Guestbook WHERE id = ?"
	const values = [guestBookPostId]
	
	db.all(query, values, function (error, guestBookPost) {
		callback(error, guestBookPost)
	})
}

exports.getAllContactPosts = function (callback) {

	const query = "SELECT * FROM Contact"

	db.all(query, function (error, viewContact) {
		callback(error, viewContact)
	})
}

exports.createContactPost = function (values, callback) {

	const query = "INSERT INTO Contact (email,topic,contactMessage) VALUES (?, ?, ?)"

	db.run(query, values, function (error) {
		callback(error);
	})
}

exports.deleteContactRequest = function (contactRequestId, callback) {

	const query = "DELETE FROM Contact WHERE id = ?"

	db.run(query, contactRequestId, function (error) {
		callback(error);
	})
}

exports.createProject = function (title, projectDescription, callback) {

	const query = "INSERT INTO Project (title,projectDescription,dateWhenPosted) VALUES (?, ?, CURRENT_TIMESTAMP)"
	const values = [title, projectDescription]

	db.run(query, values, function (error) {
		callback(error)
	})
}

exports.getAllProjects = function (callback) {

	const query = "SELECT * FROM Project"

	db.all(query, function (error, projects) {
		callback(error, projects)
	})
}

exports.deleteProjectPosts = function (projectId, callback) {

	const query = "DELETE FROM Project WHERE id = ?"

	db.run(query, projectId, function (error) {
		callback(error);
	})
}

exports.updateProject = function (id, title, projectDescription, callback) {

	const query = "UPDATE Project SET title = ?, projectDescription = ? WHERE id = ?"

	db.run(query, title, projectDescription, id, function (error) {
		callback(error)
	})
}
