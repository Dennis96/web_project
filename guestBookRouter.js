const express = require('express')
const db = require('./data')

const router = express.Router()

router.get("/", function (request, response) {

	db.getAllPosts(function (error, guestBook) {

		if (error) {
			const model = {
				somethingWentWrong: true,
				guestBook,
				csrfToken: request.csrfToken()
			}
			response.render("guestbook.hbs", model)
		}
		else {
			const model = {
				somethingWentWrong: false,
				guestBook,
				csrfToken: request.csrfToken()
			}
			response.render("guestbook.hbs", model)
		}
	})
})

router.post("/", function (request, response) {

	const data = [request.body.name, request.body.email, request.body.message]

	const validationErrors = []

	if (!request.body.name.match("^[a-z-A-Z_ ]*$")) {
		validationErrors.push("Must enter letters between a-z/A-Z.")
	}

	if (request.body.name.trim() == "") {
		validationErrors.push("Must enter valid text")
	}

	if (!request.body.email.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,14}$")) {
		validationErrors.push("Must enter a valid email.")
	}

	if (request.body.message == "") {
		validationErrors.push("Must enter a message.")
	}

	if (validationErrors.length == 0) {
		db.createGuestBookPost(data, function (error) {
			if (error) {
				response.redirect("/guestbook");
			}
			else {
				response.redirect("/guestbook");
			}
		})
	}
	else {
		const model = {
			validationErrors,
			data,
			csrfToken: request.csrfToken()
		}
		response.render("guestbook.hbs", model)
	}
})

router.post("/deleteGuestBookPost", function (request, response) {

	const guestBookId = request.body.id

	db.deleteGuestBookPost(guestBookId, function (error) {
		if (error) {
			var deleteGuestBookPostError = "Error: internal error, could not delete"

			const model = {
				deleteGuestBookPostError,
				csrfToken: request.csrfToken()
			}

			response.render("guestBook.hbs", model)
		}
		else {
			response.redirect("/guestBook")
		}
	})
})

//fixa redirect till fel /domain när du får fel. be nån smart eller dö. ERRORS DOESN't DISPLAY.
router.post("/updateGuestBookPost", function (request, response) {

	const guestBookId = request.body.id
	const name = request.body.updateName
	const email = request.body.updateEmail
	const message = request.body.updateMessages

	const data = [guestBookId, name, email, message]
	const updateErrors = []

	if (!name.match("^[a-z-A-Z_ ]*$")) {
		updateErrors.push("Must enter letters between a-z/A-Z.")
	}

	if (name.trim() == "") {
		updateErrors.push("Must enter valid text")
	}

	if (!email.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,14}$")) {
		updateErrors.push("Must enter a valid email.")
	}

	if (message == "") {
		updateErrors.push("Must enter a message ")
	}

	if (updateErrors == 0) {
		db.updateGuestBookPost(name, email, message, guestBookId, function (error) {

			if (error) {
				var updateGuestBookError = "Error: internal error, could not update"
				const model = { updateGuestBookError, csrfToken: request.csrfToken() }

				response.render("guestbook.hbs", model)
			}
			else {
				response.redirect("/guestbook")
			}
		})
	}
	else {
		const model = {
			updateErrors,
			data,
			csrfToken: request.csrfToken()
		}
		response.render("guestbook.hbs", model)
	}
})

router.get("/guestBookPost/:id", function (request, response) {

	const guestBookPostId = request.params.id
	response.cookie("previouslyViewedGuestBookPostId", guestBookPostId)

	db.getGuestBookPostById(guestBookPostId, function (error, guestBookPost) {

		if (error) {
			const model = {
				somethingWentWrong: true,
				guestBookPost,
				csrfToken: request.csrfToken()
			}
			response.render("guestBookPost.hbs", model)
		}
		else {
			const model = {
				somethingWentWrong: false,
				guestBookPost,
				csrfToken: request.csrfToken()
			}
			response.render("guestBookPost.hbs", model)
		}
	})
})

module.exports = router	