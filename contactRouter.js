const express = require('express')
const db = require('./data')

const router = express.Router()

router.get("/", function (request, response) {
	const model = { csrfToken: request.csrfToken() }
	response.render("contact.hbs", model);
});

router.get("/view-contacts", function (request, response) {

	db.getAllContactPosts(function (error, contacts) {

		if (error) {
			const model = {
				somethingWentWrong: true,
				contacts,
				csrfToken: request.csrfToken()
			}
			response.render("view-contacts.hbs", model)
		}
		else {
			const model = {
				somethingWentWrong: false,
				contacts,
				csrfToken: request.csrfToken()
			}
			response.render("view-contacts.hbs", model)
		}
	})
})

router.post("/", function (request, response) {

	const values = [request.body.email, request.body.topic, request.body.contactMessage]
	const validationErrors = []

	if (!request.body.email.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,14}$")) {
		validationErrors.push("Must enter a valid email.")
	}

	if (request.body.topic == undefined) {
		validationErrors.push("Enter a valid topic!")
	}

	if (request.body.contactMessage.trim() == "") {
		validationErrors.push("Must enter a message.")
	}

	if (validationErrors.length == 0) {

		db.createContactPost(values, function (error) {
			if (error) {
				const model = {
					createContactPostError: true,
					csrfToken: request.csrfToken()
				}
				response.redirect("/contact", model)
			}
			else {
				response.redirect("/contact");
			}
		})
	}
	else {
		const model = {
			validationErrors,
			values,
			csrfToken: request.csrfToken()
		}
		response.render("contact.hbs", model)
	}
})

router.post("/deleteContactPost", function (request, response) {

	const contactID = request.body.id

	db.deleteContactRequest(contactID, function (error) {
		if (error) {
			const model = {
				deleteContactPostError: true,
				csrfToken: request.csrfToken()
			}
			response.render("view-contacts.hbs", model)
		}
		else {
			response.redirect("/contact/view-contacts")
		}
	})
})

module.exports = router
