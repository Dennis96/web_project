const express = require("express");
const expressHandlebars = require("express-handlebars");
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const session = require('express-session')
const SQLiteStore = require('connect-sqlite3')(session)
const app = express();
const bcrypt = require('bcrypt')
const csrf = require('csurf')
const db = require('./data')

const guestBookRouter = require('./guestBookRouter')
const contactRouter = require('./contactRouter')
const projectRouter = require('./projectRouter')

const username = "Dennis"
const saltRounds = 10;
const password = "5555"
const hash = "$2b$10$LcOebxeCpIRiFLuZuVfNI.bY4qr88w1Lc4NqtLBtK8czjZP1EVK8e"

app.use(express.static("public"));

app.engine("hbs", expressHandlebars({
	defaultLayout: "main.hbs"
}));

app.use(bodyParser.urlencoded({
	extended: false
}));

app.use(cookieParser())

app.use(csrf({
	cookie: true
}))

app.use(session({
	saveUninitialized: false,
	resave: false,
	secret: 'jsadhjksadhjksadhsajk'
}))

app.use(function (request, response, next) {
	response.locals.isLoggedIn = request.session.isLoggedIn
	next()
})

app.use(function (request, response, next) {

	const previouslyViewedGuestBookPostId = request.cookies.previouslyViewedGuestBookPostId

	db.getGuestBookPostById(previouslyViewedGuestBookPostId, function () {

		response.locals.previouslyViewedGuestBookPostId = previouslyViewedGuestBookPostId

		next()
	})
})

app.use("/guestbook", guestBookRouter)
app.use("/contact", contactRouter)
app.use("/projects", projectRouter)

app.get("/", function (request, response) {
	const model = { csrfToken: request.csrfToken() }
	response.render("home.hbs", model);
});

app.get("/home", function (request, response) {
	const model = { csrfToken: request.csrfToken() }
	response.render("home.hbs", model);
});

app.get("/about", function (request, response) {
	const model = { csrfToken: request.csrfToken() }
	response.render("about.hbs", model);
});

app.get("/login", function (request, response) {
	const model = { csrfToken: request.csrfToken() }
	response.render("login.hbs", model);
})

app.get('/logout', function (request, response) {
	request.session.isLoggedIn = false
	response.redirect('/')
})

app.post("/login", function (request, response) {

	bcrypt.compare(request.body.password, hash, function (err, isMatch) {

		if (err) {
			throw err
		}
		if (!isMatch || request.body.username != username) {
			invalidCredentials = true
			const model = { invalidCredentials, csrfToken: request.csrfToken() }
			response.render("login.hbs", model)
		}
		else {
			request.session.isLoggedIn = true
			response.redirect("/")
		}
	})
})

app.listen(8080);
