const express = require('express')
const db = require('./data')

const router = express.Router()

router.get("/create-project", function (request, response) {
	const model = {
		csrfToken: request.csrfToken()}
		response.render("create-project.hbs", model);
})

router.get("/", function (request, response) {

	db.getAllProjects(function (error, project) {

		if (error) {
			const model = {
				somethingWentWrong: true,
				project, 
				csrfToken: request.csrfToken()
			}
			response.render("projects.hbs", model)
		}
		else {
			const model = {
				somethingWentWrong: false,
				project, 
				csrfToken: request.csrfToken()
			}
			response.render("projects.hbs", model)
		}
	})
})

router.post("/create-project", function (request, response) {

	const title = request.body.title
	const projectDescription = request.body.projectDescription

	const validationErrors = []

	if (!title.match("^[a-zA-Z0-9_.-]*$")) {
		validationErrors.push("Must enter a valid letter or number")
	}
	
	if (title.trim() == "") {
		validationErrors.push("Must enter a title")
	}

	if (projectDescription.trim() == "") {
		validationErrors.push("Must enter a projectDescription.")
	}

	if (validationErrors.length == 0) {

		db.createProject(title, projectDescription, function (error) {

			if (error) {
				const model = {dataBaseError: true, 
					csrfToken: request.csrfToken()	}
					response.render("./create-project.hbs", model)
			}
			else {
				response.redirect("/projects");
			}
		})
	}
	else {
		const model = {
			validationErrors,
			title,
			projectDescription, 
			csrfToken: request.csrfToken()
		}
		response.render("./create-project.hbs", model)
	}
})

router.post("/deleteProjectPost", function (request, response) {

	const projectId = request.body.id

	db.deleteProjectPosts(projectId, function (error) {
		if (error) {
			var deleteProjectPostError = "Error: internal error, could not delete"

			const model = { deleteProjectPostError, csrfToken: request.csrfToken() }

			response.render("projects.hbs", model)
		}
		else {
			response.redirect("/projects")
		}
	})
})


//fixa redirect till fel /domain när du får fel. be nån smart eller dö.
router.post("/updateProjectPost", function (request, response) {

	const projectId = request.body.id
	const title = request.body.title
	const projectDescription = request.body.projectDescription
	
	const updateError = []

	if (!title.match("^[a-zA-Z0-9_.-]*$")) {
		updateError.push("Must enter a valid letter or number as your project title")
	}
	
	if (title.trim() == "") {
		updateError.push("Must enter a title when updating")
	}

	if (projectDescription.trim() == "") {
		updateError.push("Must enter a projectDescription when updating.")
	}

	if (updateError == 0) {

		db.updateProject(projectId, title, projectDescription, function (error) {

			if (error) {
				const model = { updateProjectError, csrfToken: request.csrfToken() }

				response.render("projects.hbs", model)
			}
			else {
				response.redirect("/projects")
			}
		})
	}
	else {
		const model = {
			updateError,
			title,
			projectDescription, 
			csrfToken: request.csrfToken()
		}
		response.render("projects.hbs", model)
	}
})

module.exports = router